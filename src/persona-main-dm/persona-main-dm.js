//mode modules > lit element.js 
import { LitElement } from 'lit-element';

class PersonaMainDM extends LitElement{

    static get properties() {
        return {
            people: {type:Array}
        };
    }

    constructor() {
        super();

        this.people = [
            {
                id: 1,
                name: "Pepe Diaz",
                yearsInCompany: 10,
                profile: "Proident ad duis magna irure in et officia aliquip nisi sint eiusmod cupidatat ut. Irure incididunt fugiat qui eiusmod aute mollit reprehenderit incididunt Lorem et. Enim dolor aliqua nostrud non.",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Lisa simpson"
                }
            },{
                id: 2,
                name: "Susana Romero",
                yearsInCompany: 2,
                profile: "Proident ad duis magna irure in et officia aliquip nisi sint eiusmod cupidatat ut. Irure incididunt fugiat qui eiusmod aute mollit reprehenderit incididunt Lorem et. Enim dolor aliqua nostrud non.",    
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Lisa simpson"
                }
                
            },{
                id: 3,
                name: "Jaime Lopez",
                yearsInCompany: 5,
                profile: "Proident ad duis magna irure in et officia aliquip nisi sint eiusmod cupidatat ut. Irure incididunt fugiat qui eiusmod aute mollit reprehenderit incididunt Lorem et. Enim dolor aliqua nostrud non.",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Lisa simpson"
                }
            },{
                id: 4,
                name: "Jimmy Choo",
                yearsInCompany: 9,
                profile: "Proident ad duis magna irure in et officia aliquip nisi sint eiusmod cupidatat ut. Irure incididunt fugiat qui eiusmod aute mollit reprehenderit incididunt Lorem et. Enim dolor aliqua nostrud non.",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Lisa simpson"
                }
            },{
                id: 5,
                name: "Roxxy Valky",
                yearsInCompany: 1,
                profile: "Proident ad duis magna irure in et officia aliquip nisi sint eiusmod cupidatat ut. Irure incididunt fugiat qui eiusmod aute mollit reprehenderit incididunt Lorem et. Enim dolor aliqua nostrud non.",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Lisa simpson"
                }
            }
        ]

    }

    updated(changePorperties){
        console.log("updated main dm");

        if(changePorperties.has("people")){

            console.log("Ha cambiado la propiedad people"); 
            this.dispatchEvent(new CustomEvent("updated-people-dm", {
                detail: {
                    people: this.people
                }
            }));
        }
    }

}

customElements.define("persona-main-dm", PersonaMainDM)