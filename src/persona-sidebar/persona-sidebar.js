//mode modules > lit element 
import { LitElement, html, css } from 'lit-element';

class PersonaSidebar extends LitElement{


    static get properties(){
        return {
            peopleStats: {type: Object},
            filterYears: {type: String}
        }
    }

    constructor(){
        super();

        this.peopleStats = {};
    }


    render(){
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <h2>Persona Sidebar!</h2>
            <aside>
                <section>
                    <div>
                       Hay <span class="badge bg-pill bg-primary">${this.peopleStats.numberOfPeople}</span> personas.
                    </div>
                    <div class="mt-5">
                        <button class="w-100 btn bg-success" style="font-size: 50px"
                            @click="${this.newPerson}"><strong>+</strong></button>
                    </div>
                    <div class="mt-5">
                        <p>Filtra personas por antiguedad:</p>
                        <!--<input id="filterYearsId" type="range" min="1" max="10" value="10" @input="${this.updateYears}">-->
                        <input type="range" 
                            min="1" max="${this.peopleStats.maxYearsInCompany}" 
                            step="1"
                            value="${this.peopleStats.maxYearsInCompany}"
                            @input="${this.updateMaxYearsInCompanyFilter}">
                    </div>
                </section>
            </aside>
        `;
    }

    newPerson(e){
        console.log("Se va a crear una persona "+e);

        this.dispatchEvent(new CustomEvent("new-person", {}))
    }

    updateMaxYearsInCompanyFilter(e){
        console.log('updateMaxYearsInCompanyFilter '+ e.target.value);

        this.dispatchEvent(new CustomEvent('updated-max-years-filter', {
                detail:{
                    maxYearsInCompanyFilter: e.target.value
                }
            }
        ));
    }

    updateYears(e){
        console.log('updateYears');
        this.filterYears = this.shadowRoot.getElementById("filterYearsId").value;
        console.log('filterYears '+this.filterYears);

        this.dispatchEvent(new CustomEvent('updated-filter-years', {
                detail:{
                    filterYears: this.filterYears
                }
            }
        ));
    }
}

customElements.define("persona-sidebar", PersonaSidebar)