//mode modules > lit element.js 
import { LitElement, html } from 'lit-element';

class FichaPersona extends LitElement{

    static get properties() {
        return {
          name: { type: String },
          yearsInCompany: { type: String },
          personInfo: { type: String}
        };
    }

    constructor() {
        //siempre se llama a super(); llama al constructor de la clase padre que es Lit element
        super();
        this.name = "Introduce nombre";
        this.yearsInCompany = 12;
    }

    //ciclo de vida de LitElement
    //llega un map con las propiedades que se han cambiado - chnageProperties
    //solo sale al inicio
    updated(chnageProperties){
        console.log("Updated");

        chnageProperties.forEach((oldvalue, propName) => {
            console.log("Propiedad "+ propName + " cambia valor, anterior era "+oldvalue);
        });

        if(chnageProperties.has("name")){
            console.log("Propiedad name ha cambiado el valor, anterior era " + chnageProperties.get("name")+
             " nuevo es "+ this.name);
        }

        if(chnageProperties.has("yearsInCompany")){
            console.log("*Propiedad personInfo ha cambiado el valor, anterior era " + chnageProperties.get("personInfo")+" nuevo es "+ this.personInfo);

            this.updatePersonInfo();
        }
    }

    render(){
        return html `
            <div>
                <h1>Formulario </h1>
                <label>Nombre completo</label>
                <input type="text" id="fname" name="fname" value="${this.name}" @input="${this.updateName}"/>
                <br />
                <label>Años en la empresa</label>
                <input type="text" name="yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"/>
                <input type="text" name="infoperson" value="${this.personInfo}" disabled />
            </div>
        `;
    }

    updateName(e){
        console.log(`updateName ${e}`);
        //recupera la propiedad del input, que esta dentro de e
        this.name = e.target.value;
    }

    updateYearsInCompany(e){
        console.log(`updateYearsInCompany ${e}`);
        this.yearsInCompany = e.target.value;
    }

    updatePersonInfo(){
        console.log("updatePersonInfo");
        
        if(this.yearsInCompany >= 7){
            this.personInfo = "lead";
        }else if(this.yearsInCompany >= 5){
            this.personInfo = "Senior";
        }else if(this.yearsInCompany >= 3){
            this.personInfo =  "team";   
        }else{
            this.personInfo = "Junior";
        }

    }
}

customElements.define("ficha-persona", FichaPersona)