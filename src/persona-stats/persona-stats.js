//mode modules > lit element.js 
import { LitElement, html } from 'lit-element';

class PersonaStats extends LitElement{

    static get properties() {
        return {
            people: {type: Array}
        };
    }

    updated(changePorperties){
        console.log("updated Stats");

        if(changePorperties.has("people")){
            console.log("Ha cambiado la propiedad people"); 

            let peopleStats = this.getherPeopleArrayInfo(this.people);
            this.dispatchEvent(new CustomEvent("updated-people-stats", {
                detail: {
                    peopleStats: peopleStats
                }
            }));
        }
    }

    constructor() {
        super();
        this.people = [];
    }

    getherPeopleArrayInfo(people){
        let peopleStats = {};
        peopleStats.numberOfPeople = people.length;
        console.log("getherPeopleArrayInfo Long array:"+peopleStats.numberOfPeople);

        let maxYearsInCompany = 0;
        people.forEach(person => {
            if(person.yearsInCompany >= maxYearsInCompany){
                maxYearsInCompany = person.yearsInCompany
            }
        });

        console.log("MaxYearsInCompany es "+maxYearsInCompany);
        peopleStats.maxYearsInCompany = maxYearsInCompany;

        return peopleStats;
    }

}

customElements.define("persona-stats", PersonaStats)