//mode modules > lit element.js 
import { LitElement, html } from 'lit-element';

class PersonaHeader extends LitElement{

    static get properties() {
        return {
        };
    }

    constructor() {
        //siempre se llama a super(); llama al constructor de la clase padre que es Lit element
        super();
    }

    render(){
        return html `
            <div>
               <!-- <h2>PersonaHeader</h2> -->
            </div>
        `;
    }

}

customElements.define("persona-header", PersonaHeader)