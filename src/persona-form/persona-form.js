//mode modules > lit element 
import { LitElement, html } from 'lit-element';

class PersonaForm extends LitElement{

    static get properties() {
        return {
            person: {type: Object},
            editPerson: {type: Boolean}
        };
    }

    constructor() {
        super();
        this.resetFormData();
    }

    render(){
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <div style="padding: 10px">
                <form>
                    <div class="form-group">
                        <label>Nombre completo</label>
                        <input type="text" class="form-control" placeholder="Nombre completo" 
                        @input="${this.updateName}"
                        .value="${this.person.name}" 
                        ?disabled="${this.editPerson}"/>
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea class="form-control" placeholder="Perfil" rows="5"
                        @input="${this.updateProfile}" 
                        .value="${this.person.profile}"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input type="text" class="form-control" placeholder="Años en la empresa"
                        @input="${this.updateYearsInCompany}"
                        .value="${this.person.yearsInCompany}">
                    </div>
                    <br/>
                    <button @click="${this.goBack}" name="" id="" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storePerson}" name="" id="" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>
            
        `;
    }

    updateName(e){
        console.log("updateName");
        console.log("Actualizando la propiedad name con el valor "+e.target.value);

        this.person.name = e.target.value;
    }

    updateProfile(e){
        console.log("updateProfile");
        console.log("Actualizando la propiedad updateProfile con el valor "+e.target.value);

        this.person.profile = e.target.value;
    }

    updateYearsInCompany(e){
        console.log("updateYearsInCompany");
        console.log("Actualizando la propiedad updateYearsInCompany con el valor "+e.target.value);

        this.person.yearsInCompany = e.target.value;
    }

    goBack(e){
        console.log("goBack");
        e.preventDefault();

        this.dispatchEvent(new CustomEvent("persona-form-close", {}));

        this.resetFormData();
    }

    storePerson(e){
        console.log("storePerson - Guardando persona...");
        e.preventDefault();

        console.log("la propiedad name de la persona: "+this.person.name);
        console.log("la propiedad profile de la persona: "+this.person.profile);
        console.log("la propiedad yearsInCompany de la persona: "+this.person.yearsInCompany);
   
        this.person.photo = {
            "src": "./img/default-user.jpg",
            "alt": "Persona nueva"
        }

        this.dispatchEvent(new CustomEvent("persona-form-store",{
                detail: {
                    person: { //pueden ser otros nombres, mientras los manejeres en los otros componentes con esos mismos nombres
                        id: this.person.id,
                        name: this.person.name,
                        profile: this.person.profile,
                        yearsInCompany: this.person.yearsInCompany,
                        photo: this.person.photo
                    },
                    editPerson: this.editPerson
                }
            }
        ));
    }

    resetFormData(){
        console.log("resetFormData");

        this.person = {};
        this.person.id = "";
        this.person.name = "";
        this.person.profile = "";
        this.person.yearsInCompany = "";

        this.editPerson = false;
    }

}

customElements.define("persona-form", PersonaForm)