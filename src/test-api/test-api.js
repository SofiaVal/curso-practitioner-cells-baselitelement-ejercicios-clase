//mode modules > lit element 
import { LitElement, html } from 'lit-element';

class TestApi extends LitElement{

    static get properties(){
        return{
            movies: {type: Array},
            loading: {type: Boolean}
        };
    }

    constructor(){
        super();

        this.movies = [];
        this.getMovieData();
    }

    render(){
        return html `
            <h3>Test api</h3>
            ${this.movies.map(
                        movie =>  html `<div> La pelicula ${movie.title} fue dirigida por ${movie.director}</div>`  
                )
            }
        `;
    }

    getMovieData(){
        console.log("Obtener datos de las peliculas");

        //llamada AJAX
        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if(xhr.status === 200){
                console.log("Peticion correcta.")

                let APIResponse = JSON.parse(xhr.responseText);
                console.log(APIResponse);

                this.movies = APIResponse.results;
            }
        }

        xhr.open("GET", "https://swapi.dev/api/films/");
        xhr.send();
        console.log("Fin de getMovieData()");

    }


}

customElements.define("test-api", TestApi)

/*
        fetch('https://swapi.dev/api/films/')
            .then(response => response.json())
            .then((jsonResponse) => {
                this.movies = jsonResponse.films;
                console.log('Peliculas: '+this.movies);
                this.loading=false;
            });
*/