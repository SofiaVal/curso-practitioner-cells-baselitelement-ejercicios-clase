//mode modules > lit element.js 
import { LitElement, html, css } from 'lit-element';

class PersonaFooter extends LitElement{

    static get styles(){
        return css `
           div h3 {
                text-align: center;
            }
        `;
    }

    static get properties() {
        return {
        };
    }

    constructor() {
        //siempre se llama a super(); llama al constructor de la clase padre que es Lit element
        super();
    }

    render(){
        return html `
            <div>
                <h3>PersonaFooter @SofíaValera 2021</h3>
            </div>
        `;
    }

}

customElements.define("persona-footer", PersonaFooter)