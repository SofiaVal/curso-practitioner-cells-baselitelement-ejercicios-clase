//mode modules > lit element.js 
import { LitElement, html } from 'lit-element';
//no hace falta poner el .JS
import '../persona-header/persona-header';
import '../persona-main/persona-main';
import '../persona-footer/persona-footer';
import '../persona-sidebar/persona-sidebar';
import '../persona-stats/persona-stats';
import '../persona-main-dm/persona-main-dm';

class PersonaApp extends LitElement{

    static get properties() {
        return {
            people: {type: Array},
            filterYears: {type: String}
        };
    }

    constructor() {
        //siempre se llama a super(); llama al constructor de la clase padre que es Lit element
        super();
    }

    updated(changePorperties){
        console.log("updated");

        if(changePorperties.has("people")){
            console.log("Ha cambiado la propiedad people");
            this.shadowRoot.querySelector("persona-stats").people = this.people;
        }

        if(changePorperties.has("filterYears")){
            console.log("Ha cambiado la propiedad filterYears");
            console.log(this.filterYears)
        }
    }

    render(){
        return html `
            <div>
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
                <!-- <h1>PersonaApp</h1> -->
                <persona-header></persona-header>
                <div class="row">
                    <persona-sidebar @new-person="${this.newPerson}"  
                        @updated-max-years-filter="${this.newMaxYearsInCompanyFilter}"
                        @updated-filter-years="${this.updateYears}" class="col-2">
                    </persona-sidebar>
                    <persona-main .filterYears="${this.filterYears}" @updated-people="${this.updatedPeople}" class="col-10" ></persona-main>
                </div>
                <persona-footer></persona-footer>
                <persona-stats @updated-people-stats="${this.updatedPeopleStats}"></persona-stats>
               <!-- <persona-main-dm @updated-people-dm="${this.updatePepleDM}"></persona-main-dm> -->
            </div>
        `;
    }

    /*updatePepleDM(e){
        console.log("updatePepleDM")
        console.log(e.detail)
        this.shadowRoot.querySelector("persona-main").people = e.detail.people;
    }*/

    newPerson(e){
        console.log("newPerson en persona-app");
        console.log(e);
    
        //es como document
        this.shadowRoot.querySelector("persona-main").showPersonForm = true;
    }

    newMaxYearsInCompanyFilter(e){
        console.log("newMaxYearsInCompanyFilter "+e.detail.maxYearsInCompanyFilter);
        this.shadowRoot.querySelector("persona-main").maxYearsInCompanyFilter = e.detail.maxYearsInCompanyFilter;
    }

    updatedPeople(e){
        console.log("updatedPeople");
        this.people = e.detail.people;
    }

    updatedPeopleStats(e){
        console.log("updatedPeopleStats");
        console.log(e.detail);

        this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;
        this.shadowRoot.querySelector("persona-main").maxYearsInCompanyFilter = e.detail.peopleStats.maxYearsInCompany;
    }

    updateYears(e){
        console.log("updateYears filter");
        this.filterYears = e.detail.filterYears;
        console.log(this.filterYears);
    }

}

customElements.define("persona-app", PersonaApp)