//mode modules > lit element.js 
import { LitElement, html, css } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado';
import '../persona-form/persona-form';
import '../persona-main-dm/persona-main-dm'
class PersonaMain extends LitElement{

    static get styles(){
        return css `
            :host {
                all: initial;
            }
        `;
    }

    static get properties() {
        return {
            people: {type:Array},
            showPersonForm: {type: Boolean},
            maxYearsInCompanyFilter: {type:String}
        };
    }

    constructor() {
        //siempre se llama a super(); llama al constructor de la clase padre que es Lit element
        super();
        this.showPersonForm=false;
        this.people=[];
       /* this.people = [
            {
                id: 1,
                name: "Pepe Diaz",
                yearsInCompany: 10,
                profile: "Proident ad duis magna irure in et officia aliquip nisi sint eiusmod cupidatat ut. Irure incididunt fugiat qui eiusmod aute mollit reprehenderit incididunt Lorem et. Enim dolor aliqua nostrud non.",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Lisa simpson"
                }
            },{
                id: 2,
                name: "Susana Romero",
                yearsInCompany: 2,
                profile: "Proident ad duis magna irure in et officia aliquip nisi sint eiusmod cupidatat ut. Irure incididunt fugiat qui eiusmod aute mollit reprehenderit incididunt Lorem et. Enim dolor aliqua nostrud non.",    
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Lisa simpson"
                }
                
            },{
                id: 3,
                name: "Jaime Lopez",
                yearsInCompany: 5,
                profile: "Proident ad duis magna irure in et officia aliquip nisi sint eiusmod cupidatat ut. Irure incididunt fugiat qui eiusmod aute mollit reprehenderit incididunt Lorem et. Enim dolor aliqua nostrud non.",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Lisa simpson"
                }
            },{
                id: 4,
                name: "Jimmy Choo",
                yearsInCompany: 9,
                profile: "Proident ad duis magna irure in et officia aliquip nisi sint eiusmod cupidatat ut. Irure incididunt fugiat qui eiusmod aute mollit reprehenderit incididunt Lorem et. Enim dolor aliqua nostrud non.",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Lisa simpson"
                }
            },{
                id: 5,
                name: "Roxxy Valky",
                yearsInCompany: 1,
                profile: "Proident ad duis magna irure in et officia aliquip nisi sint eiusmod cupidatat ut. Irure incididunt fugiat qui eiusmod aute mollit reprehenderit incididunt Lorem et. Enim dolor aliqua nostrud non.",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Lisa simpson"
                }
            }
        ]*/
       
    }

    render(){
        return html `
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <h2 class="text-center">Personas (Main)</h2>
            <div class="row" id="peopleList">
                <!-- Si es muy pequeño 4 columns si no 1-->
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.people.filter(
                        person => parseInt(person.yearsInCompany) <= parseInt(this.maxYearsInCompanyFilter)
                    ).map(
                        person => html `<persona-ficha-listado 
                            id="${person.id}"
                            fname="${person.name}"
                            yearsInCompany="${person.yearsInCompany}"
                            profile="${person.profile}"
                            .photo="${person.photo}"
                            @delete-person="${this.deletePerson}"
                            @info-peron="${this.infoPerson}"
                            >
                        </persona-ficha-listado>`
                    )}
                </div>
            </div>
            <div class="row">
                <persona-form id="personForm" class="d-none border rounded border-primary"
                @persona-form-close="${this.personaFormClose}"
                @persona-form-store="${this.personFormStore}"></persona-form>
            </div>
            <persona-main-dm @updated-people-dm="${this.updatePepleDM}"></persona-main-dm>
        `;
    }

    updated(changedProperties){
        console.log("Updated MAIN");

        if(changedProperties.has("showPersonForm")){
            console.log("Ha cambiado el valor de la propiedad de showPersonForm: "+this.showPersonForm);

            this.showPersonForm ? this.showPersonFormData()  : this.showPersonList();
        }

        if(changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad people");

            this.dispatchEvent(new CustomEvent('updated-people', {
                    detail:{
                        people: this.people
                    }
                }
            ));

            console.log(this.people);
        }

        if(changedProperties.has("maxYearsInCompanyFilter")){
            console.log("updated maxYearsInCompanyFilter");
            console.log("antiguedad maxima: "+this.maxYearsInCompanyFilter);
        }
    }

    updatePepleDM(e){
        console.log("updatePepleDM")
        console.log(e.detail)

        this.people = e.detail.people;
    }

    personFormStore(e){
        console.log("personFormStore");
        console.log("Almacenar persona");
        
        if(e.detail.editPerson){
            console.log("Se va a actualizar la persona de nombre  "+e.detail.person.name+", con ID: "+e.detail.person.id);
        
            this.people = this.people.map(
                person => person.id === e.detail.person.id
                    ? person = e.detail.person  : person
            );

        }else{

            let idNuevo = this.people.length+1;
            e.detail.person.id = idNuevo;
            console.log("Id: "+e.detail.person.id);
            
            //this.people.push(e.detail.person);
            this.people = [...this.people, e.detail.person]

            console.log("Persona almacenada nueva.");

        }

        this.showPersonForm = false;
    }

    personaFormClose(){
        console.log("personaFormClose");
        console.log("Se ha cerrado el formulario de la persona.");
        this.showPersonForm = false;
    }

    showPersonList(){
        console.log("showPersonList");
        console.log("Mostrando el listado de personas");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
    }

    showPersonFormData(){
        console.log("showPersonFormData");
        console.log("Mostrando el formulario de personas");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }

    deletePerson(e){
        console.log("deletePerson en persona-main"+ e.detail.name+" con id="+e.detail.id);

        this.people = this.people.filter(
            person => person.id != e.detail.id         // person => person.name != e.detail.name
        );

    }

    infoPerson(e){
        console.log("InfoPerson");
        console.log("Se ha pedido mas informacion de la persona " + e.detail.name + ", id: "+e.detail.id);
        
        //person => person.name === e.detail.name
        let choosedPerson = this.people.filter(
            person => person.id === e.detail.id
        )

        let person = {};
        person.id = choosedPerson[0].id;
        person.name = choosedPerson[0].name;
        person.yearsInCompany = choosedPerson[0].yearsInCompany;
        person.profile = choosedPerson[0].profile;

        //hay que poner un nombre que este como properties para que no de problemas al cambiar o actualizar esa pripiedad
        //ya que al no estar registrado como propiedad no lo va a poder actualizar

        this.shadowRoot.getElementById("personForm").person = person;
        this.shadowRoot.getElementById("personForm").editPerson = true;
        this.showPersonForm = true;
        
    }

}

customElements.define("persona-main", PersonaMain)