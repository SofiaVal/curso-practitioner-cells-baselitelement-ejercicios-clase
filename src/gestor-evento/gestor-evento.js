//mode modules > lit element 
import { LitElement, html } from 'lit-element';
import '../emisor-evento/emisor-evento';
import '../receptor-evento/receptor-evento';

class GestorEvento extends LitElement{

    static get properties(){
        return{
        };
    }

    constructor(){
        super();
    }

    render(){
        return html `
            <h1>Gestor evento</h1>
            <emisor-evento @test-event="${this.processEvent}"></emisor-evento>
            <receptor-evento id="receiver"></receptor-evento>
        `;
    }

    processEvent(e){
        console.log("processEvent");
        console.log(e);

        //no lleva document, porque queremos acceder al DOM local que es el shadowroot
        this.shadowRoot.getElementById("receiver").course = e.detail.course;
        this.shadowRoot.getElementById("receiver").year = e.detail.year;
    }
}

customElements.define("gestor-evento", GestorEvento)